# SegmentedProgress

![](segaction.gif)

Assign SegmentedProgressView as your view class. 

```swift
// setup initial and progress color
progressV.secondaryColor = .lightGray
progressV.primaryColor = .green

//segment width will be determined by segmentSeparationPoint and numberOfSegments
progressV.segmentSeparationPoint = 12
progressV.numberOfSegments = 16

//set the progress with/without animation
progressV.setProgress(0.2, animated: true)
```
