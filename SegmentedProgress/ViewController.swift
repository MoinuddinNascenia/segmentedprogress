//
//  ViewController.swift
//  SegmentedProgress
//
//  Created by S.M.Moinuddin on 30/9/18.
//  Copyright © 2018 S.M.Moinuddin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak private var progressV: SegmentedProgressView!
    private var progress:CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // setup initial and progress color
        progressV.secondaryColor = .lightGray
        progressV.primaryColor = .green
        
        //segment width will be determined by segmentSeparationPoint and numberOfSegments
        progressV.segmentSeparationPoint = 12
        progressV.numberOfSegments = 16
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction private func increaseAction(_ sender: UIButton) {
        progress += 0.1
        if progress > 1 {progress = 1; return}
        progressV.setProgress(progress, animated: true)
    }

    @IBAction private func decreaseAction(_ sender: UIButton) {
        progress -= 0.1
        if progress < 0 {progress = 0; return}
        progressV.setProgress(progress, animated: true)
    }

}

