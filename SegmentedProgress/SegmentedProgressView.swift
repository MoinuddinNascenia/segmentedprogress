//
//  SegmentedProgressView.swift
//  SegmentedProgress
//
//  Created by S.M.Moinuddin on 30/9/18.
//  Copyright © 2018 S.M.Moinuddin. All rights reserved.
//

import UIKit

class SegmentedProgressView: UIView {
    
    private(set) var progress:CGFloat = 0
    
    private var segmentsLayer = CAShapeLayer()
    private var segmentColorsPrimary = [UIColor]()
    private var segmentColorsBackground = [UIColor]()
    
    var cornerRadius: CGFloat = 0
    private var animationDuration: CGFloat = 0
    private var animationFromValue: CGFloat = 0
    private var animationToValue: CGFloat = 0
    private var animationStartTime: CFTimeInterval = 0
    private var displayLink: CADisplayLink?
    
    
    private var primaryColors: [UIColor]? {
        didSet{
            resetColorsForSegments()
        }
    }
    private var secondaryColors: [UIColor]? {
        didSet{
            resetColorsForSegments()
        }
    }
    
    var primaryColor = UIColor.orange {
        didSet{
            resetColorsForSegments()
            setNeedsDisplay()
        }
    }
    
    var secondaryColor = UIColor.darkGray {
        didSet{
            resetColorsForSegments()
            setNeedsDisplay()
        }
    }
    
    var segmentSeparationPoint: CGFloat = 0 {
        didSet{
            setNeedsDisplay()
        }
    }
    
    var numberOfSegments = 0 {
        didSet{
            //First remove all the sub layers
            segmentsLayer.sublayers = nil;
            //Then add sub layers equal to the number of segments
            for  _ in 0..<numberOfSegments {
                let segment = CAShapeLayer()
                segment.frame = self.bounds
                segmentsLayer.addSublayer(segment)
            }
            //Reset the colors for the segements
            resetColorsForSegments()
            setNeedsDisplay()
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        backgroundColor = .clear
        
        //ProgressLayer
        segmentsLayer.frame = bounds
        layer.addSublayer(segmentsLayer)
        
        //Set defauts
        animationDuration = 0.3
        numberOfSegments = 16
        segmentSeparationPoint = 15.0
        cornerRadius = 2.0
        
        //Layout
        layoutSubviews()
    }
    
    func setProgress(_ progress:CGFloat, animated:Bool) {
        if (animated == false) {
            if (displayLink != nil) {
                //Kill running animations
                displayLink?.invalidate()
                displayLink = nil
            }
            self.progress = progress
            setNeedsDisplay()
        } else {
            animationStartTime = CACurrentMediaTime()
            animationFromValue = self.progress
            animationToValue = progress
            if (displayLink == nil) {
                //Create and setup the display link
                displayLink?.invalidate()
                displayLink = CADisplayLink(target: self, selector: #selector(animateProgress))
                displayLink?.add(to: .current, forMode: .commonModes)
            } /*else {
             //Reuse the current display link
             }*/
        }
    }
    
    //MARK:- Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        segmentsLayer.frame = self.bounds
        guard let subLayers = segmentsLayer.sublayers else {return}
        for _layer in subLayers {
            _layer.frame = self.bounds
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIViewNoIntrinsicMetric, height: UIViewNoIntrinsicMetric)
    }
    
    
    //MARK:- Drawing
    
    private var numberOfFullSegments: Int{
        let num = (self.progress * CGFloat(numberOfSegments)).rounded()
        
        return Int(num)
    }
    
    private func colorForSegment(_ index: Int) -> UIColor {
        if (index < numberOfFullSegments) {
            return segmentColorsPrimary[index]
        } else {
            return segmentColorsBackground[index];
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        drawProgress()
    }
    
    private func resetColorsForSegments() {
        if primaryColors == nil {
            var tempArray = [UIColor]()
            for _ in 0..<numberOfSegments {
                tempArray.append(primaryColor)
            }
            segmentColorsPrimary = tempArray
        }
        
        
        if let primaryColors = primaryColors, (numberOfSegments == primaryColors.count) {
            segmentColorsPrimary = primaryColors
        }
        
        if let primaryColors = primaryColors, (numberOfSegments != primaryColors.count && primaryColors.count != 0) {
            var tempArray = [UIColor]()
            for i in 0..<numberOfSegments {
                if (numberOfSegments > primaryColors.count) {
                    tempArray.append(primaryColors[i])
                } else {
                    tempArray.append(primaryColors[i % primaryColors.count])
                }
            }
            segmentColorsPrimary = tempArray
        }
        
        if secondaryColors == nil {
            var tempArray = [UIColor]()
            for _ in 0..<numberOfSegments {
                tempArray.append(secondaryColor)
            }
            segmentColorsBackground = tempArray
        }
        
        if let secondaryColors = secondaryColors, (numberOfSegments == secondaryColors.count) {
            segmentColorsBackground = secondaryColors
        }
        
        if let secondaryColors = secondaryColors, (numberOfSegments != secondaryColors.count && secondaryColors.count != 0) {
            var tempArray = [UIColor]()
            for i in 0..<numberOfSegments {
                if (numberOfSegments > secondaryColors.count) {
                    tempArray.append(secondaryColors[i])
                } else {
                    tempArray.append(secondaryColors[i % secondaryColors.count])
                }
            }
            segmentColorsBackground = tempArray
        }
    }
    
    @objc private func animateProgress(_ displayLink: CADisplayLink) {
        DispatchQueue.main.async { [unowned self] in
            let dt = CGFloat(displayLink.timestamp - self.animationStartTime) / self.animationDuration
            if (dt >= 1.0) {
                //Order is important! Otherwise concurrency will cause errors, because setProgress: will detect an animation in progress and try to stop it by itself. Once over one, set to actual progress amount. Animation is over.
                self.displayLink?.invalidate()
                self.displayLink = nil
                self.progress = self.animationToValue
                self.setNeedsDisplay()
                return
            }
            
            //Set progress
            self.progress = self.animationFromValue + dt * (self.animationToValue - self.animationFromValue)
            self.setNeedsDisplay()
        }
    }
    
    private func drawProgress() {
        var segmentWidth:CGFloat = 0
        segmentWidth = (self.bounds.size.width - (CGFloat(numberOfSegments - 1) * segmentSeparationPoint)) / CGFloat(numberOfSegments)
        
        //Iterate through all the segments that are full.
        for i in 0..<numberOfSegments {
            let rect = CGRect(x: CGFloat(i) * (segmentWidth + segmentSeparationPoint), y: 0, width: segmentWidth, height: bounds.size.height)
            let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
            
            //Add segment to the proper layer, and color it
            guard let shapeLayer = segmentsLayer.sublayers![i] as? CAShapeLayer else {return}
            shapeLayer.path = path.cgPath
            shapeLayer.fillColor = colorForSegment(i).cgColor
        }
        
    }
    
}
